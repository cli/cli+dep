# file      : makefile
# author    : Boris Kolpackov <boris@codesynthesis.com>
# copyright : Copyright (c) 2009-2011 Code Synthesis Tools CC
# license   : MIT; see accompanying LICENSE file

include $(dir $(lastword $(MAKEFILE_LIST)))/build-0.3/bootstrap.make

default  := $(out_base)/

.PHONY: $(default) test install clean

# Build.
#
$(default): $(out_base)/cli/cli/

# Test.
#
test: $(out_base)/libcutl/.test $(out_base)/cli/.test

# Install.
#
install: $(out_base)/cli/.install

# Clean.
#
clean: $(out_base)/libcutl/.clean $(out_base)/cli/.clean

src_root := $(src_base)/libcutl
scf_root := $(src_root)/build
out_root := $(src_root)
$(call import,$(src_base)/libcutl/makefile)

src_root := $(src_base)/cli
scf_root := $(src_root)/build
out_root := $(src_root)
$(call import,$(src_base)/cli/makefile)
